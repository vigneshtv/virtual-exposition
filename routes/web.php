<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Blade::setContentTags('<%', '%>');        // for variables and all things Blade
Blade::setEscapedContentTags('<%%', '%%>');   // for escaped data

//VIEWS

Route::get('/', function () {
    return view('home');
});

Route::get('/event/create', function () {
    return view('createevent');
});

Route::get('/event/{eventId}/stands', function ($eventId) {
    return view('stands')->with('eventId',$eventId);
})->middleware('resolveEvent');

Route::get('/event/{eventId}/createstand', function ($eventId) {
    return view('createstand')->with('eventId',$eventId);
})->middleware('resolveEvent');

Route::get('/event/{eventId}/stand/{standId}/reserve', function ($eventId,$standId) {
    return view('reservestand')->with('eventId',$eventId)->with('standId',$standId);
})->middleware('resolveEvent')->middleware('resolveStand');

Route::get('/event/{eventId}/stand/{standId}/showstand', function ($eventId,$standId) {
    return view('showstand')->with('eventId',$eventId)->with('standId',$standId);
})->middleware('resolveEvent')->middleware('resolveStand');

Route::get('/event/{eventId}/sendreport', function ($eventId) {
    return view('sendreport')->with('eventId',$eventId);
})->middleware('resolveEvent');

//JSON ENDPOINTS

Route::post('/event/createevent', [
    'uses' => 'RequestController@createEvent'
]);

Route::post('/event/{eventId}/standcreate', [
    'uses' => 'RequestController@createStand'
]);

Route::get('event/{eventId}/show', [
    'uses' => 'RequestController@showEvent'
])->middleware('resolveEvent');

Route::post('event/{eventId}/sendreports', [
    'uses' => 'RequestController@sendReports'
])->middleware('resolveEvent');

Route::get('eventlist', [
    'uses' => 'RequestController@allEvents'
]);

Route::get('event/{eventId}/stand/{standId}/show', [
    'uses' => 'RequestController@showStand'
])->middleware('resolveEvent')->middleware('resolveStand');

Route::get('event/{eventId}/standlist', [
    'uses' => 'RequestController@allStands'
])->middleware('resolveEvent');

Route::post('event/{eventId}/stand/{standId}/reservestand', [
    'uses' => 'RequestController@reserveStand'
])->middleware('resolveEvent')->middleware('resolveStand');
