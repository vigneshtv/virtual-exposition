<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\DB;
use Closure;

class ResolveStand
{

  public function handle($request, Closure $next) {
    $standId = $request->input('standId');
    $results = DB::select('select standInfo.eventId,standInfo.standId,standInfo.standName,standInfo.standDescription,standInfo.imageUrl,standInfo.status, registrations.displayName,registrations.email,registrations.phone,registrations.address,registrations.companyAdminEmail,registrations.companyLogo,registrations.marketingDoc,registrations.bookDate from standInfo LEFT JOIN registrations ON standInfo.standId = registrations.standId where standInfo.standId = :standId', ['standId' => $standId]);
    if(count($results)!=0){
      $request->request->add(['stand' => $results[0]]);
    }else{
      //$request->event = '';
    }

      return $next($request);
  }

}
