<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\DB;
use Closure;

class ResolveEvent
{

  public function handle($request, Closure $next) {
    $eventId = $request->input('eventId');
    $results = DB::select('select * from eventInfo where eventId = :eventId', ['eventId' => $eventId]);
    if(count($results)!=0){
      $request->request->add(['event' => $results[0]]);
    }else{
      //$request->event = '';
    }

      return $next($request);
  }

}
