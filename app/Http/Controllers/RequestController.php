<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware('guest');
    }

    public function showEvent(Request $request){
      $event = $request->input('event');
      return response()->json(['event'=>$event]);
    }

    public function allStands(Request $request){
      $eventId = $request->input('eventId');
      $event = $request->input('event');
      $eventName = $request->input('eventName');

      $standResults = DB::select('select standInfo.eventId,standInfo.standId,standInfo.standName,standInfo.standDescription,standInfo.imageUrl,standInfo.status, registrations.displayName,registrations.email,registrations.phone,registrations.address,registrations.companyAdminEmail,registrations.companyLogo,registrations.marketingDoc,registrations.bookDate from standInfo LEFT JOIN registrations ON standInfo.standId = registrations.standId where standInfo.eventId = :eventId', ['eventId' => $eventId]);
      return response()->json(['stands'=>$standResults,'event'=>$event]);
    }

    public function showStand(Request $request){
      $event = $request->input('event');
      $stand = $request->input('stand');

      return response()->json(['stand'=>$stand,'event'=>$event]);
    }

    public function allEvents(Request $request){
      $results = DB::select('select * from eventInfo',[]);
      return response()->json($results);
    }

    public function reserveStand(Request $request){
      $eventId = $request->eventId;
      $standId = $request->standId;
      $displayName = $request->input('displayName');
      $email = $request->input('email');
      $phone = $request->input('phone');
      $adminEmail = $request->input('adminEmail');
      $companylogo = '';
      $marketingdoc = '';

      if ($request->file('companylogo')->isValid() && $request->file('marketingdoc')->isValid()) {
        $companylogo = $request->file('companylogo')->store('public/logo');
        $marketingdoc = $request->file('marketingdoc')->store('public/docs');

        $companylogo = '/storage'.strstr($companylogo, '/');
        $marketingdoc = '/storage'.strstr($marketingdoc, '/');
      }


      if(DB::insert('insert into registrations (eventId, standId, displayName, email, phone, companyAdminEmail, companyLogo, marketingDoc) values (?, ?, ?, ?, ?, ?, ?, ?)', [$eventId, $standId, $displayName, $email, $phone, $adminEmail, $companylogo, $marketingdoc])){
        if(DB::update('update standInfo set status = 0 where standId = ?', [$standId])){
          return response()->json(['status'=>'success']);
        }else{
          return response()->json(['status'=>'failure']);
        }
      }
    }

    public function createEvent(Request $request){
      $eventName = $request->eventName;
      $lat = $request->lat;
      $lon = $request->lon;
      $eventDate = $request->eventDate;
      $eventSummary = $request->eventSummary;
      $eventAddress = $request->eventAddress;
      $eventId = $this->generateRandomString();

      if(DB::insert('insert into eventInfo (eventId,eventName,lat,lon,eventDate,eventSummary,eventAddress) values (?,?,?,?,?,?,?)', [$eventId,$eventName,$lat,$lon,$eventDate,$eventSummary,$eventAddress])){
        return response()->json(['status'=>'success','eventId'=>$eventId]);
      }else{
        return response()->json(['status'=>'failure']);
      }
    }

    public function createStand(Request $request){
      $eventId = $request->eventId;
      $standId = $this->generateRandomString();
      $standName = $request->standName;
      $price = $request->price;
      $standDescription = $request->standDescription;

      $imageUrl = '';

      if ($request->file('standImage')->isValid()) {
        $imageUrl = $request->file('standImage')->store('public/standimage');
        $imageUrl = '/storage'.strstr($imageUrl, '/');
      }

      if(DB::insert('insert into standInfo (eventId,standId,status,standName,price,standDescription,imageUrl) values (?,?,?,?,?,?,?)', [$eventId,$standId,'1',$standName,$price,$standDescription,$imageUrl])){
        return response()->json(['status'=>'success','standId'=>$standId]);
      }else{
        return response()->json(['status'=>'failure']);
      }
    }

    public function generateRandomString($length = 20) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    public function sendReports(Request $request){
      $adminEmail = $request->adminEmail;
      $eventId = $request->eventId;
      $event = $request->event;

      $eventResult = DB::select('select * from eventInfo where eventId = :eventId', ['eventId' => $eventId]);
      $standResults = DB::select('select * from standInfo where eventId = :eventId', ['eventId' => $eventId]);
      $regResults = DB::select('select * from registrations where eventId = :eventId', ['eventId' => $eventId]);

      $standCount = count($standResults);
      $regCount = count($regResults);

      // $event = $eventResult[0];
      $subject = "Reports for Event ".$event->eventName ;
      $message = '<html>
        <head>
          <title>Reports for event '.$event->eventName.'</title>
        </head>
        <body>
          <table>
            <tr>
              <td>Event Name:</td>
              <td>'.$event->eventName.'</td>
            </tr>
            <tr>
              <td>Number of Stands:</td>
              <td>'.$standCount.'</td>
            </tr>
            <tr>
              <td>Date of Event:</td>
              <td>'.$event->createdDate.'</td>
            </tr>
            <tr>
              <td>Number of Reservations:</td>
              <td>'.$regCount.'</td>
            </tr>
          </table>
        </body>
        </html>';
      if($this->sendMail("no-reply@gmail.com",$adminEmail,$subject,$message)){
        return response()->json(['status'=>'success','msg'=>'Mail sent to '.$adminEmail.' with subject '.$subject]);
      }else{
        return response()->json(['status'=>'failure','msg'=>'Cannot send mail to '.$adminEmail.' for '.$event->eventName]);
      }
    }

    public function sendMail($from,$to,$subject,$message){
      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
      $headers .= 'To: You <'.$to.'>'."\r\n";
      $headers .= 'From: '.$from."\r\n";
      $headers .= 'Reply-To: '.$from."\r\n";
      $headers .= 'X-Mailer: PHP/'.phpversion();
      // $headers .= 'Cc: myboss@example.com' . "\r\n";
      return mail($to,$subject,$message,$headers);
    }
}
