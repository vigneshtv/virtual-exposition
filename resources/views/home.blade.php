@extends('layouts.resource')

@section('title', 'Virtual Exposition')

@section('header')
@endsection

@section('content')
    <div layout="column" data-ng-controller="HomeController" data-ng-init="allEvents()" ng-cloak>

      <md-toolbar class="md-warn">
        <div class="md-toolbar-tools">
          <h2 class="md-flex">Virtual Exposition</h2>
          <span flex></span>
            <a href="/event/create">
               <md-button class="md-raised" aria-label="Add Event">
               Add Events
             </md-button>
            </a>
        </div>
      </md-toolbar>

      <md-content flex layout-padding>
        <p>
          <ng-map center="current-location" zoom-to-include-markers="auto" style="height:80vh !important;">
            <marker ng-repeat="event in events" position="[{{event.lon}},{{event.lat}}]" title="{{event.eventName}}" on-click="markerClicked('{{event.eventId}}')"></marker>
          </ng-map>
        </p>

        <p>

        </p>

      </md-content>

      <span class="md-caption">Made by T.V.Vignesh (vigneshviswam@gmail.com) as a project for evaluation purposes only.</span>
    </div>
@stop
