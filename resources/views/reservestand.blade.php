@extends('layouts.resource')

@section('title', 'Stands - Virtual Exposition')

@section('header')
@endsection

@section('content')
    <div layout="column" data-ng-controller="StandController" data-ng-init="showStand('<% $eventId %>','<% $standId %>')" ng-cloak>

      <md-toolbar class="md-warn">
        <div class="md-toolbar-tools">
          <h2 class="md-flex">Reserve Stand {{stand.standName}} in {{event.eventName}}</h2>
          <span flex></span>
            <a href="/">
               <md-button class="md-raised" aria-label="HOME">
               HOME
             </md-button>
            </a>
        </div>
      </md-toolbar>

      <md-content flex layout-padding>

      <form name="standReserveForm">

        <div layout-gt-sm="row">
          <md-input-container class="md-block" flex-gt-xs>
            <label>Your Name</label>
            <input ng-model="registration.displayName" name="displayName">
          </md-input-container>

          <md-input-container class="md-block" flex-gt-xs>
            <label>Your Email</label>
            <input ng-model="registration.email" type="email" name="email">
          </md-input-container>

          <md-input-container class="md-block" flex-gt-xs>
            <label>Your Contact Number</label>
            <input ng-model="registration.phone" name="phone">
          </md-input-container>
        </div>

        <div layout-gt-sm="row">
          <md-input-container class="md-block" flex-gt-xs>
            <label>Company Admin Email</label>
            <input ng-model="registration.adminEmail" type="email" name="adminEmail">
          </md-input-container>
        </div>

        <div layout-gt-sm="row">
          <span>
            <input type="file" class="ng-hide" ng-model-instant id="companylogo" onchange="angular.element(this).scope().setFiles(this,'companylogo')" accept="image/*"/>
            <label for="companylogo" class="md-button md-raised md-primary">Attach Company Logo</label>
          </span>
          <span>
            <input type="file" class="ng-hide" ng-model-instant id="marketingdoc" onchange="angular.element(this).scope().setFiles(this,'marketingdoc')"/>
            <label for="marketingdoc" class="md-button md-raised md-primary">Attach Marketing Documents</label>
          </span>
        </div>

      <br>
      <md-button class="md-raised md-warn" ng-click="reservestand()">Reserve Stand</md-button>

      </form>
      </md-content>

    </div>
@stop
