@extends('layouts.resource')

@section('title', 'Send Report - Virtual Exposition')

@section('header')
@endsection

@section('content')
    <div layout="column" data-ng-controller="StandController" ng-init="showEvent(<% $eventId %>)" ng-cloak>

      <md-toolbar class="md-warn">
        <div class="md-toolbar-tools">
          <h2 class="md-flex">Send Reports</h2>
          <span flex></span>
            <a href="/">
               <md-button class="md-raised" aria-label="HOME">
               HOME
             </md-button>
            </a>
        </div>
      </md-toolbar>

      <md-content flex layout-padding>

      <form name="reportForm">

        <div layout-gt-sm="row">
          <md-input-container class="md-block" flex-gt-xs>
            <label>Email Address to Receive Report</label>
            <input ng-model="adminEmail" name="adminEmail">
          </md-input-container>
        </div>

      <br>
      <md-button class="md-raised md-warn" ng-click="sendReport('<% $eventId %>')">Send Reports</md-button>

      </form>
      </md-content>

    </div>
@stop
