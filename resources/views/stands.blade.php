@extends('layouts.resource')

@section('title', 'Stands - Virtual Exposition')

@section('header')
@endsection

@section('content')
    <div layout="column" data-ng-controller="StandController" data-ng-init="allStands('<% $eventId %>')" ng-cloak>

      <md-toolbar class="md-warn">
        <div class="md-toolbar-tools">
          <h2 class="md-flex">Stands in {{event.eventName}}</h2>
          <span flex></span>
            <a href="/event/<% $eventId %>/createstand">
               <md-button class="md-raised" aria-label="Add Stand">
               Add Stand
             </md-button>
           </a>
            <a href="/">
               <md-button class="md-raised" aria-label="HOME">
               HOME
             </md-button>
            </a>
        </div>
      </md-toolbar>

      <md-content flex layout-padding>

        <div ng-show="stands.length===0">
          Hmm.. Looks like no stands has been added to this event yet. Why don't you try Adding!
        </div>

          <md-card data-ng-repeat="stand in stands">
            <md-card-title>
              <md-card-title-text>
                <span class="md-headline">
                  {{stand.standName}}
                  <span class="bgm-lightgreen md-caption standStatus" ng-show="stand.status==1">Free</span>
                  <span class="bgm-red md-caption standStatus" ng-show="stand.status==0">Booked</span>
                </span>
                <span class="md-subhead">{{stand.standDescription}}</span>

                <br>
                <div data-ng-show="stand.price">
                  Price : {{stand.price}}$
                </div>
                <br>
                <div data-ng-show="stand.companyAdminEmail">
                  Admin Email : {{stand.companyAdminEmail}}
                </div>

                <div data-ng-show="stand.marketingDoc">
                  Marketing Documents : <a href="{{stand.marketingDoc}}" download="">Click to Download</a>
                </div>

              </md-card-title-text>
              <md-card-title-media>
                <div class="md-media-lg card-media" data-ng-show="stand.companyLogo && stand.status==0">
                  <img ng-src="{{stand.companyLogo}}" alt="{{stand.companyLogo}}" class="standImage"/>
                </div>
                <div class="md-media-lg card-media" data-ng-show="stand.imageUrl && stand.status==1">
                  <img ng-src="{{stand.imageUrl}}" alt="{{stand.imageUrl}}" class="standImage"/>
                </div>
              </md-card-title-media>

            </md-card-title>

            <md-card-content data-ng-show="stand.status==0">
              This stand was booked on {{prettyDateOnly(stand.bookDate)}} by
              <a href="mailto:{{stand.email}}">
                <md-tooltip>
                  Send an email to {{stand.displayName}} - {{stand.email}}
                </md-tooltip>
                {{stand.displayName}}
              </a>
            </md-card-content>

            <md-card-content data-ng-show="stand.status==1">
              This stand is available to be booked. Go ahead and click on Reserve to book this stand.
            </md-card-content>

            <md-card-actions layout="row" layout-align="end center">
              <a href="/event/{{event.eventId}}/stand/{{stand.standId}}/reserve">
                <md-button ng-show="stand.status==1">
                  RESERVE
                  <md-tooltip>
                    Reserve this stand for your company
                  </md-tooltip>
                </md-button>
              </a>
            </md-card-actions>
          </md-card>



        <p>

        </p>

      </md-content>

    </div>
@stop
