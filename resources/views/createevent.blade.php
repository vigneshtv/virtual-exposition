@extends('layouts.resource')

@section('title', 'Create Event - Virtual Exposition')

@section('header')
@endsection

@section('content')
    <div layout="column" data-ng-controller="HomeController" ng-cloak>

      <md-toolbar class="md-warn">
        <div class="md-toolbar-tools">
          <h2 class="md-flex">Create Event</h2>
          <span flex></span>
            <a href="/">
               <md-button class="md-raised" aria-label="HOME">
               HOME
             </md-button>
            </a>
        </div>
      </md-toolbar>

      <md-content flex layout-padding>

      <form name="eventForm">

        <div layout-gt-sm="row">
          <md-input-container class="md-block" flex-gt-xs>
            <label>Event Name</label>
            <input ng-model="editEvent.eventName" name="eventName">
          </md-input-container>
        </div>

        <div layout-gt-sm="row">

          <md-input-container class="md-block" flex-gt-xs>
            <label>Latitude</label>
            <input ng-model="editEvent.lat" name="lat">
          </md-input-container>

          <md-input-container class="md-block" flex-gt-xs>
            <label>Longitude</label>
            <input ng-model="editEvent.lon" name="lon">
          </md-input-container>

          <md-input-container class="md-block" flex-gt-xs>
            <label>Event Date</label>
            <md-datepicker ng-model="editEvent.eventDate" name="eventDate"></md-datepicker>
          </md-input-container>
        </div>

        <div layout-gt-sm="row">
          <md-input-container class="md-block" flex-gt-xs>
            <label>Event Summary</label>
            <input ng-model="editEvent.eventSummary" name="eventSummary">
          </md-input-container>
        </div>

        <div layout-gt-sm="row">
          <md-input-container class="md-block" flex-gt-xs>
            <label>Event Address</label>
            <input ng-model="editEvent.eventAddress" name="eventAddress">
          </md-input-container>
        </div>

      <br>
      <md-button class="md-raised md-warn" ng-click="createEvent()">Create Event</md-button>

      </form>
      </md-content>

    </div>
@stop
