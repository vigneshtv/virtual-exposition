<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Library Styles -->
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/bower_components/angular-material/angular-material.min.css" />

    <!-- Custom Styles -->
    <link rel="stylesheet" href="/css/style.css" />

    <!-- Library Scripts -->
    <script type="text/javascript" src="/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyBwbmF8RMG_a0U3LM_7HIwj5zO5ili1AYI"></script>
    <script type="text/javascript" src="/bower_components/angular/angular.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-aria/angular-aria.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-animate/angular-animate.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-material/angular-material.min.js"></script>
    <script src="/bower_components/ngmap/build/scripts/ng-map.min.js"></script>

    <!-- Angular Controller Scripts -->
    <script type="text/javascript" src="/js/controllers/main.controller.js"></script>
    <script type="text/javascript" src="/js/controllers/home.controller.js"></script>
    <script type="text/javascript" src="/js/controllers/stand.controller.js"></script>
</head>

  <body>

    <div class="container" ng-controller="MainController">
      <input type="hidden" name="_token" id="csrfToken" value="<% csrf_token() %>">
        @yield('content')
    </div>

  </body>
</html>
