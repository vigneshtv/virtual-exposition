@extends('layouts.resource')

@section('title', 'Create Event - Virtual Exposition')

@section('header')
@endsection

@section('content')
    <div layout="column" data-ng-controller="StandController" ng-init="showEvent('<% $eventId %>')" ng-cloak>

      <md-toolbar class="md-warn">
        <div class="md-toolbar-tools">
          <h2 class="md-flex">Create Stand</h2>
          <span flex></span>
            <a href="/event/<% $eventId %>/stands">
               <md-button class="md-raised" aria-label="BACK TO STANDS">
               BACK TO EVENTS
             </md-button>
            </a>
            <a href="/">
               <md-button class="md-raised" aria-label="HOME">
               HOME
             </md-button>
            </a>
        </div>
      </md-toolbar>

      <md-content flex layout-padding>

      <form name="standForm">

        <div layout-gt-sm="row">
          <md-input-container class="md-block" flex-gt-xs>
            <label>Stand Name</label>
            <input ng-model="editStand.standName" name="standName">
          </md-input-container>
        </div>

        <div layout-gt-sm="row">

          <md-input-container class="md-block" flex-gt-xs>
            <label>Stand Description</label>
            <input ng-model="editStand.standDescription" name="standDescription">
          </md-input-container>

        </div>

        <div layout-gt-sm="row">
          <md-input-container class="md-block" flex-gt-xs>
            <label>Price</label>
            <input ng-model="editStand.price" name="price">
          </md-input-container>
        </div>

        <div layout-gt-sm="row">
          <span>
            <input type="file" class="ng-hide" ng-model-instant id="standImage" onchange="angular.element(this).scope().setFiles(this,'standImage')" accept="image/*"/>
            <label for="standImage" class="md-button md-raised md-primary">Attach Stand Image</label>
          </span>
        </div>

      <br>
      <md-button class="md-raised md-warn" ng-click="createStand()">Create Stand</md-button>

      </form>
      </md-content>

    </div>
@stop
