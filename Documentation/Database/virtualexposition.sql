-- phpMyAdmin SQL Dump
-- version 4.6.4deb1+deb.cihar.com~xenial.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 31, 2016 at 02:46 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

--
-- By T.V.Vignesh
--
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `virtualexposition`
--
CREATE DATABASE IF NOT EXISTS `virtualexposition` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `virtualexposition`;

-- --------------------------------------------------------

--
-- Table structure for table `eventInfo`
--

DROP TABLE IF EXISTS `eventInfo`;
CREATE TABLE `eventInfo` (
  `eventId` varchar(50) NOT NULL COMMENT 'ID of the event',
  `eventName` varchar(200) NOT NULL COMMENT 'Name of the event',
  `lat` float NOT NULL COMMENT 'Latitude',
  `lon` float NOT NULL COMMENT 'Longitude',
  `eventDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Event Date',
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of Event Creation',
  `eventSummary` text NOT NULL COMMENT 'Summary of the event',
  `eventAddress` text NOT NULL COMMENT 'Address for the event'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table having Event information';

--
-- Dumping data for table `eventInfo`
--

INSERT INTO `eventInfo` (`eventId`, `eventName`, `lat`, `lon`, `eventDate`, `createdDate`, `eventSummary`, `eventAddress`) VALUES
('1234', 'Test event', 80.1931, 12.971, '2016-10-30 18:30:00', '2016-10-28 19:57:40', 'Test event summary', 'Test event address'),
('ep626mc3bcrvrTCnHd46', 'Event 2', 80.1986, 12.9852, '2016-10-30 17:47:48', '2016-10-30 17:47:48', 'Testing event', 'Test event address');

-- --------------------------------------------------------

--
-- Table structure for table `registrations`
--

DROP TABLE IF EXISTS `registrations`;
CREATE TABLE `registrations` (
  `eventId` varchar(50) NOT NULL COMMENT 'Event ID',
  `standId` varchar(50) NOT NULL COMMENT 'Stand ID',
  `displayName` varchar(200) NOT NULL COMMENT 'Display Name',
  `email` varchar(200) NOT NULL COMMENT 'Email Address',
  `phone` varchar(15) NOT NULL COMMENT 'Phone Number',
  `address` text COMMENT 'Address',
  `companyAdminEmail` varchar(200) NOT NULL COMMENT 'Company Admin Email ID',
  `companyLogo` varchar(200) NOT NULL COMMENT 'Company Logo URL',
  `marketingDoc` varchar(200) NOT NULL COMMENT 'Marketing DOC URL',
  `bookDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of creation'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='TABLE HAVING STAND REGISTRATION INFO';

--
-- Dumping data for table `registrations`
--

INSERT INTO `registrations` (`eventId`, `standId`, `displayName`, `email`, `phone`, `address`, `companyAdminEmail`, `companyLogo`, `marketingDoc`, `bookDate`) VALUES
('1234', '1111', 'Vignesh T.V', 'vigneshviswam@gmail.com', '9841030866', NULL, 'tvvnarayanan@gmail.com', '/storage/logo/da5a412eb1cad930603566b9b8233760.jpeg', '/storage/docs/d754e28b94bf66a97633eb1da47a7317.docx', '2016-10-30 16:55:57'),
('1234', '1212', 'T.V.Vignesh', 'vigneshviswam@gmail.com', '9841030866', 'No.15, Balaji Nagar, 10th street, Nanganallur, Chennai - 600061', 'tvpulla@gmail.com', 'https://www.google.com/logos/doodles/2015/googles-new-logo-5078286822539264.3-hp2x.gif', '', '2016-10-30 07:01:23'),
('ep626mc3bcrvrTCnHd46', 'dNtEG61oj6udB34UfYbN', 'Vignesh T.V', 'vigneshviswam@gmail.com', '9841030866', NULL, 'tvvnarayanan@gmail.com', '/storage/logo/a6a94c19aa6956bb926a23e1753b3615.jpeg', '/storage/docs/1ac80467ba54beba4cda2f1a4766ebbc.jpeg', '2016-10-30 21:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `standInfo`
--

DROP TABLE IF EXISTS `standInfo`;
CREATE TABLE `standInfo` (
  `eventId` varchar(50) NOT NULL COMMENT 'Event ID',
  `standId` varchar(50) NOT NULL COMMENT 'Stand ID',
  `status` tinyint(1) NOT NULL COMMENT 'True-Active, False-Disabled',
  `standName` varchar(200) NOT NULL COMMENT 'Name of the stand',
  `price` float NOT NULL COMMENT 'Price of the stand',
  `standDescription` text NOT NULL COMMENT 'Stand Description',
  `imageUrl` varchar(200) NOT NULL COMMENT 'Image URL',
  `standCreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date the stand was created'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='TABLE HAVING DETAILS OF STANDS';

--
-- Dumping data for table `standInfo`
--

INSERT INTO `standInfo` (`eventId`, `standId`, `status`, `standName`, `price`, `standDescription`, `imageUrl`, `standCreatedDate`) VALUES
('1234', '1111', 0, 'Stand 2', 500, 'This is description of stand 2', '', '2016-10-30 18:34:02'),
('ep626mc3bcrvrTCnHd46', 'dNtEG61oj6udB34UfYbN', 0, 'Testing new stand 2', 500, 'Stand description 2', '/storage/standimage/ac31c4caf85b2e3c310f88e574b4621d.png', '2016-10-30 18:34:02'),
('ep626mc3bcrvrTCnHd46', 'LwzWXnfKoAIBncg4iitC', 1, 'Testing stand 3', 100, 'Stand desc', '/storage/standimage/64017987386c19ca47b47d229da2170f.jpeg', '2016-10-30 18:34:02'),
('ep626mc3bcrvrTCnHd46', 'TvyQjcUhxMkLbrOiN0lY', 1, 'Testing new stand', 200, 'Stand description', '/storage/standimage/177e309d345e124695f4eab2fe1371d8.png', '2016-10-30 18:34:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `eventInfo`
--
ALTER TABLE `eventInfo`
  ADD PRIMARY KEY (`eventId`);

--
-- Indexes for table `registrations`
--
ALTER TABLE `registrations`
  ADD PRIMARY KEY (`eventId`,`standId`,`email`);

--
-- Indexes for table `standInfo`
--
ALTER TABLE `standInfo`
  ADD PRIMARY KEY (`eventId`,`standId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
