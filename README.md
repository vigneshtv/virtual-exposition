The project is not a complete implementation of the given scenario but is only meant for evaluating my technical skillset. 

All the requirements as asked have been met.

TECHNOLOGIES USED
---------------------

PHP Framework - Laravel 5.3 with Blade Templating Engine

Client Side Framework - AngularJS 1.5

Libraries - JQuery, Material Angular, FontAwesome

Database - MySQL

Server - Apache 2.4

Communication Channel - JSON

IDE - Atom

PHP Version - 7.0

Unit Testing - phpunit

Package Manager - Bower

Database UI - PHPMyAdmin

OS Used - Ubuntu 16.04


INSTALLATION INSTRUCTIONS
----------------------

1) Get laravel installed. Instructions here: https://laravel.com/docs/5.3

2) All packages are included in this folder. If anything misses out, it can be installed by doing: "bower install". If you dont have bower installed in your system, you can follow these instructions: https://bower.io/#install-bower

3) The SQL file has been provided along with this folder which can be imported onto the system (I generally use PHPMyAdmin to do that for me)

4) Create a user providing all necessary previleges to the database and edit the .env file inside virtualexposition directory providing the respective username and password

4) From the folder virtualexposition and run "php artisan serve"

5) Visit localhost:8000 from the browser (Default for artisan serve)

6) Incase you want to receive mails (optional), make sure you have a mail server (sendmail or postfix) configured on the system. Instructions here: https://www.digitalocean.com/community/tutorials/how-to-install-and-setup-postfix-on-ubuntu-14-04

7) Unit Tests are available under virtualexposition/tests/UnitTest.php and can be run with phpunit (100% test pass)


ASSUMPTIONS & FUTURE SCOPE TO THIS PROJECT
-------------------------------------------

Considering that this project is for evaluation purposes only, these features have not been implemented (It was also not asked in requirements):

1) Editing an Event,Stand,Registration

2) Deleting an Event,Stand,Registration

3) Login System for Admins,Users,etc. (All of them have access to do anything as of now)

4) Validation of fields do not exist now.


POSITIVES OF THE APP
---------------------------

1) Responsive & Mobile Friendly design

2) Consistent use of Material Design guidelines

3) Backend is robust due to the use of Laravel Middlewares and DB Facades

4) Use of web standards where possible

5) Modular code and templating which makes most of the sections re-usable
