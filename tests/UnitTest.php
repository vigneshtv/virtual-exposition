<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;



class UnitTest extends TestCase
{
    /**
     * Home Page Test
     *
     * @return void
     */
    public function testHomePage()
    {
        $this->visit('/')->see('Virtual');
    }

    public function generateRandomString($length = 20) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    /*
    * Add Event Test
    */
    public function testAddEvent()
    {
        $eventId = $this->generateRandomString();
        $this->assertTrue(DB::insert('insert into eventInfo (eventId,eventName,lat,lon,eventDate,eventSummary,eventAddress) values (?,?,?,?,?,?,?)', [$eventId,'Test Event','10','15','2016-10-31 00:00:00','Test Event summary','Event address']));
    }

    /*
    * Show Event Test
    */
    public function testShowEvent()
    {
        $results = DB::select('select * from eventInfo');
        if(count($results)>0){
          $eventId = $results[0]->eventId;
          $this->visit('/event/'.$eventId.'/stands')->see('/event/'.$eventId.'/createstand');
        }else{
          $this->assertTrue(true);
        }
    }

    /*
    * Add Stand Test
    */
    public function testAddStand()
    {
        $results = DB::select('select * from eventInfo');
        if(count($results)>0){
          $eventId = $results[0]->eventId;
          $standId = $this->generateRandomString();
          $this->assertTrue(DB::insert('insert into standInfo (eventId,standId,status,standName,price,standDescription,imageUrl) values (?,?,?,?,?,?,?)', [$eventId,$standId,'1','Test Stand','100','Test Description','']));
        }else{
          $this->assertTrue(true);
        }
    }

    /*
    * Show Stand Test
    */
    public function testShowStand()
    {
        $results = DB::select('select * from standInfo');
        if(count($results)>0){
          $eventId = $results[0]->eventId;
          $standId = $results[0]->standId;
          $this->visit('/event/'.$eventId.'/stand/'.$standId.'/showstand')->see('showStand(\''.$eventId.'\',\''.$standId.'\')');
        }else{
          $this->assertTrue(true);
        }
    }

    /*
    * Reserve Stand Test
    */
    public function testReserveStand()
    {
        $results = DB::select('select * from standInfo');
        if(count($results)>0){
          $eventId = $results[0]->eventId;
          $standId = $results[0]->standId;

          if(DB::insert('insert into registrations (eventId, standId, displayName, email, phone, companyAdminEmail, companyLogo, marketingDoc) values (?, ?, ?, ?, ?, ?, ?, ?)', [$eventId, $standId, 'T.V.Vignesh', 'vigneshviswam@gmail.com', '22243697', 'tvvnarayanan@gmail.com', '', ''])){
            if(DB::update('update standInfo set status = 0 where standId = ?', [$standId])){
              $this->assertTrue(true);
            }else{
              $this->assertTrue(false);
            }
          }else{
            $this->assertTrue(false);
          }
        }else{
          $this->assertTrue(true);
        }
    }
}
