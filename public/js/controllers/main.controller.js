var myApp = angular.module('myApp',['ng','ngAnimate','ngAria','ngMaterial','ngMap']);

myApp.controller('MainController', ['$scope','$mdToast',function($scope,$mdToast) {
  $scope.test = "hello";

  $scope.ajaxRequest = function(url,data,method,success,error){
    var jqxhr = $.ajax({
      url : url,
      type : method,
      data : data
    }).done(success).fail(error);
  };

  $scope.prettyDateOnly = function(dateObj){
    if(!dateObj||dateObj==='')return null;
    var date = new Date(dateObj);
    if(date.getFullYear()===(new Date(0).getFullYear())){
      return 'Date not specified';
    }
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var monthNames = ['January', 'February', 'March','April', 'May', 'June', 'July','August', 'September', 'October','November', 'December'];
    return day+', '+monthNames[monthIndex]+' '+year;
  };

  $scope.prettyTime = function(dateObj){
			if(!dateObj||dateObj==='')return null;
			var date = new Date(dateObj);

			if(date.getFullYear()===(new Date(0).getFullYear())){
				return ' -- : -- ';
			}

			var day = date.getDate();
			var minutes = $scope.zeroPad(date.getMinutes(),2);
			var hours = $scope.zeroPad(date.getHours(),2);

			return hours + ' : ' + minutes;
		};

    $scope.zeroPad = function(num, size) {
	    var s = num+'';
	    while (s.length < size) s = '0' + s;
	    return s;
		};

    $scope.showSimpleToast = function(content,position,delay) {
      if(!delay)delay = 3000;
      if(!position)position = 'top';
      $mdToast.show(
        $mdToast.simple()
          .textContent(content)
          .position(position)
          .hideDelay(delay)
      );
    };

}]);
