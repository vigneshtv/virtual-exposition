  myApp.controller('StandController', ['$scope','$mdBottomSheet',function($scope,$mdBottomSheet) {

    $scope.registration = {};

    $scope.allStands = function(eventId){
      //_token:$("#csrfToken").val()
      $scope.ajaxRequest('standlist',{eventId:eventId},'GET',function(data){
        console.log(data);
        $scope.$apply(function () {
          $scope.stands = data.stands;
          $scope.event = data.event;
        });
      },function(err){
        console.log(err.responseText);
      });
    };

    $scope.showEvent = function(eventId){
      $scope.ajaxRequest('/event/'+eventId+'/show',{eventId:eventId},'GET',function(data){
        console.log(data);
        $scope.$apply(function () {
          $scope.event = data.event;
        });
      },function(err){
        console.log(err.responseText);
      });
    };

    $scope.showStand = function(eventId,standId){
      $scope.ajaxRequest('/event/'+eventId+'/stand/'+standId+'/show',{eventId:eventId,standId:standId},'GET',function(data){
        console.log(data);
        $scope.$apply(function () {
          $scope.stand = data.stand;
          $scope.event = data.event;
        });
      },function(err){
        console.log(err.responseText);
      });
    };

    $scope.setFiles = function(element,key) {
    $scope.$apply(function($scope) {
      console.log(key,'files:', element.files);
      // Turn the FileList object into an Array
        $scope.files = $scope.files||{};
        $scope.files[key] = $scope.files[key] || [];
        for (var i = 0; i < element.files.length; i++) {
          $scope.files[key].push(element.files[i])
        }
        $scope.progressVisible = false
      });
    };

    $scope.createStand = function(){
      var fd = new FormData();
      for (var i in $scope.files["standImage"]) {
          fd.append("standImage", $scope.files["standImage"][i]);
      }

      var other_data = $('form').serializeArray();
      $.each(other_data,function(key,input){
          fd.append(input.name,input.value);
      });
      fd.append('_token',$("#csrfToken").val());

      $.ajax({
          url: '/event/'+$scope.event.eventId+'/standcreate',
          data: fd,
          type: 'POST',
          contentType: false,
          processData: false
      }).done(function(data){
        console.log(data);
        if(data.status === 'success'){
          $scope.showSimpleToast('The stand has been created successfully!');
          window.location.assign('/event/'+$scope.event.eventId+'/stand/'+data.standId+'/showstand');
        }
      }).fail(function(err){
        console.log(err.responseText);
        $scope.showSimpleToast('OOPS! An error occured in creating your stand');
      });

    };

    $scope.reservestand = function(){
      var fd = new FormData();
      for (var i in $scope.files["marketingdoc"]) {
          fd.append("companylogo", $scope.files["companylogo"][i]);
      }
      for (var i in $scope.files["marketingdoc"]) {
          fd.append("marketingdoc", $scope.files["marketingdoc"][i]);
      }

      var other_data = $('form').serializeArray();
      $.each(other_data,function(key,input){
          fd.append(input.name,input.value);
      });
      fd.append('_token',$("#csrfToken").val());

      $.ajax({
          url: '/event/'+$scope.event.eventId+'/stand/'+$scope.stand.standId+'/reservestand',
          data: fd,
          type: 'POST',
          contentType: false,
          processData: false
      }).done(function(data){
        console.log(data);
        if(data.status === 'success'){
          $scope.showSimpleToast('The stand has been reserved successfully!');
          window.location.assign('/event/'+$scope.event.eventId+'/stand/'+$scope.stand.standId+'/showstand');
        }
      }).fail(function(err){
        console.log(err.responseText);
        $scope.showSimpleToast('OOPS! An error occured in registering your stand');
      });

    };


    $scope.sendReport = function(eventId){
      var fd = new FormData();

      var formData = $('form').serializeArray();
      $.each(formData,function(key,input){
          fd.append(input.name,input.value);
      });
      fd.append('_token',$("#csrfToken").val());
      fd.append('eventId',eventId);

      $.ajax({
          url: '/event/'+eventId+'/sendreports',
          data: fd,
          type: 'POST',
          contentType: false,
          processData: false
      }).done(function(data){
        console.log(data);
        if(data.status === 'success'){
          $scope.showSimpleToast('The reports has been sent successfully!');
          window.location.assign('/');
        }
      }).fail(function(err){
        console.log(err.responseText);
        $scope.showSimpleToast('OOPS! An error occured in sending the reports');
      });
    };

  }]);
