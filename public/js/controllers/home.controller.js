  myApp.controller('HomeController', ['$scope','NgMap','$mdBottomSheet',function($scope,NgMap,$mdBottomSheet) {

   $scope.allEvents = function(){
     //_token:$("#csrfToken").val()
       $scope.ajaxRequest('eventlist',{},'GET',function(data){
         console.log(data);
         $scope.events = data;
         console.log('evt');
       },function(err){
         console.log(err.responseText);
       });
   };

   $scope.markerClicked = function(event,eventId){
     $scope.ajaxRequest('/event/'+eventId+'/show',{eventId:eventId},'GET',function(data){
       $scope.event = data.event;
       console.log(data);
       $scope.showEventBottomSheet();
     },function(err){
       console.log(err.responseText);
     });
   };

   $scope.showEventBottomSheet = function() {
       $scope.alert = '';
       $mdBottomSheet.show({
         templateUrl: '/templates/event-bottomsheet.html',
         controller: 'EventListBottomSheetCtrl',
         locals: {
           Event: $scope.event
         }
       }).then(function(clickedItem) {
         console.log(clickedItem);
       });
   };

   $scope.createEvent = function(){
     var fd = new FormData();

     var formData = $('form').serializeArray();
     $.each(formData,function(key,input){
         fd.append(input.name,input.value);
     });
     fd.append('_token',$("#csrfToken").val());

     $.ajax({
         url: '/event/createevent',
         data: fd,
         type: 'POST',
         contentType: false,
         processData: false
     }).done(function(data){
       console.log(data);
       if(data.status === 'success'){
         $scope.showSimpleToast('The event has been reserved successfully!');
         window.location.assign('/');
       }
     }).fail(function(err){
       console.log(err.responseText);
       $scope.showSimpleToast('OOPS! An error occured in creating your event');
     });

   };

 }])

.controller('EventListBottomSheetCtrl', function($scope, $mdBottomSheet, Event) {
  console.log(Event);
  $scope.event = Event;
});
